<?php

namespace ThinkerSoft\Database;

class DependencyException extends pgException {
    function __construct() { parent::__construct("Deadlock."); }
}
