<?php

namespace ThinkerSoft\Database;

/**
 * Класс для работы с базой даных PostgreSQL
 * @package ThinkerSoft\Database
 */
class pgDatabase
{

    protected $_connection;
    protected $_config;

    /**
     * Конструктор класса
     *
     * @param array $config массив, содержащий данные для подключения к БД
     * @throws pgException
     */
    public function __construct(array $config)
    {
        if (empty($config))
            throw new pgException(null, 101);
        $this->_config = $config;
    }

    /**
     * Деструктор класса
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Устанавливает соединение с БД
     *
     * @throws pgException
     */
    public function connect()
    {
        if ($this->_connection)
            return;
        //options='--client_encoding=UTF8'
        $connection_string = strtr("host=_hostname dbname=_database user=_username password=_password", $this->_config);
        // Создаем соединение с БД
        $this->_connection = pg_connect($connection_string);
        if ($this->_connection === false) {
            throw new pgException(null, 102);
        }
    }

    /**
     * Разрывает соединение с БД
     *
     * @return bool
     */
    public function disconnect()
    {
        $this->_connection = null;
        return true;
    }

    /**
     * Выполняет переданный SQL запрос с заданными параметрами
     *
     * @param string $sql строка запроса
     * @param array|null $params данные запроса
     * @return array
     * @throws DependencyException
     * @throws pgException
     */
    public function query($sql, $params = null)
    {
        if (!isset($this->_connection)) {
            $this->connect();
        }

        // выполняем запрос
        $result = pg_query($this->_connection, $this->compile($sql, $params));

        // проверим результат
        if ($result === false) {
            $error = pg_last_error($this->_connection);
            if (stripos($error, 'deadlock detected') !== false)
                throw new DependencyException();
            throw new pgException("$error: $sql");
        }

        // соберем то, что вернул запрос в масив
        $out = array();
        while (($d = pg_fetch_assoc($result)) !== false) {
            $out[] = $d;
        }

        return $out;
    }

    /**
     * Копирует массив строк в заданную таблицу
     *
     * @param string $table_name имя таблицы
     * @param array $rows массив строк
     * @param string $delimiter разделитель полей в строке
     * @param string $null_as NULL идентификатор
     * @return bool
     * @throws DependencyException
     * @throws pgException
     */
    public function copyFrom($table_name, array $rows, $delimiter = "\t", $null_as = "\\N")
    {
        if (!isset($this->_connection)) {
            $this->connect();
        }

        $result = pg_copy_from($this->_connection, $table_name, $rows, $delimiter, $null_as);

        if ($result === false) {
            $error = pg_last_error($this->_connection);
            if (stripos($error, 'deadlock detected') !== false)
                throw new DependencyException();
            throw new pgException($error);

        }

        return $result;
    }

    /**
     * Компилирует строку запроса, подставляя значения параметров
     *
     * @param string $sql строка запроса
     * @param array $params массив параметров
     * @return string
     */
    public function compile($sql, $params)
    {
        $sqlstr = trim(preg_replace('/\s+/', ' ', $sql));
        if (isset($params) && !empty($params) && !is_null($params)) {
            $values = array_map(array($this, 'quote'), $params);
            $sqlstr = strtr($sqlstr, $values);
        }
        return $sqlstr . (substr($sqlstr, -1) !== ';' ? ';' : '');
    }

    public function quote($value)
    {
        if ($value === NULL) {
            return 'NULL';
        } elseif ($value === TRUE) {
            return "'1'";
        } elseif ($value === FALSE) {
            return "'0'";
        } elseif (is_array($value)) {
            return '(' . implode(', ', array_map(array($this, __FUNCTION__), $value)) . ')';
        } elseif (is_int($value)) {
            return (int)$value;
        } elseif (is_float($value)) {
            return sprintf('%F', $value);
        }

        return "E'" . preg_replace('/\t/', "\\t",
            preg_replace('/\n/', "\\n", preg_replace('/\r/', "\\r",
                pg_escape_string($this->_connection, $value)))) . "'";
    }

    /**
     * Проверяет занята ли БД
     *
     * @return bool
     */
    public function isBusy()
    {
        if (!$this->_connection)
            return false;
        return (pg_connection_busy($this->_connection) ||
            (pg_transaction_status($this->_connection) !== PGSQL_TRANSACTION_IDLE));
    }
}