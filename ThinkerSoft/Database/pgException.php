<?php

namespace ThinkerSoft\Database;

use Exception;

class pgException extends Exception
{

    protected $errors = array(
        101 => "Error creating pqDatabase. Configuration database array not specified.",
        102 => "Can't connect to database server."
    );

    function __construct($msg, $id = 0, $e = null)
    {
        if (($id > 0) && empty($msg)) {
            if (isset($this->errors[$id]))
                parent::__construct($this->errors[$id], $id, $e);
            else
                parent::__construct('Unknown error.', 0, $e);
        } else {
            parent::__construct($msg, $id, $e);
        }
    }
}
