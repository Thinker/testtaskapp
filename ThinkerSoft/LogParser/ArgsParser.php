<?php

namespace ThinkerSoft\LogParser;

class ArgsParser
{

    /**
     * Парсит аргументы коммандной строки
     *
     * @return array
     * @throws ArgsParserException
     */
    public function parse()
    {
        $result = array(
            'log1' => null,
            'log2' => null,
            'truncate' => false,
            'verbose' => false
        );

        $shortOpts = "t";   // truncate table
        $shortOpts .= "v";  // verbose

        $longOpts = array(
            "log1:",    // лог файл типа 1
            "log2:"     // лог файл типа 2
        );
        // получаем опции командной строки
        $options = getopt($shortOpts, $longOpts);

        if (count($options) < 2) {
            $this->printHelp();
            throw new ArgsParserException(null, 401);
        }

        // проверяем, что передано и обрабатывем

        // передано, что необходима очистка таблиц
        if (isset($options['t'])) {
            $result['truncate'] = true;
        }

        // передано, что необходим подробый вывод информации
        if (isset($options['v'])) {
            $result['verbose'] = true;
        }

        // передано имя лог файла типа 1
        if (isset($options['log1'])) {
            $result['log1'] = $options['log1'];
        }

        // передано имя лог файла типа 2
        if (isset($options['log2'])) {
            $result['log2'] = $options['log2'];
        }

        return $result;
    }

    /**
     * Выводит помощь
     */
    private function printHelp()
    {
        echo "Usage: php logimport.php [-t] [-v] [--log1 <file>] [--log2 <file>]\n\n"
            . "where options include:\n"
            . "   --log1 <file>   path to log file type 1\n"
            . "   --log2 <file>   path to log file type 2\n"
            . "   -t              trucate destination table\n"
            . "   -v              verbose\n"
            . "Author: Pavel Thinker Kuznetsov (thinker.kms@gmail.com).\n";
    }
}