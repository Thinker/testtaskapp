<?php

namespace ThinkerSoft\LogParser;

use Exception;

class ArgsParserException extends Exception {

    protected $errors = array(
        401 => "Not enough parameters.",
    );

    function __construct($msg, $id = 0, $e = null) {
        if (($id>0)&&empty($msg)) {
            if (isset($this->errors[$id]))
                parent::__construct($this->errors[$id], $id, $e);
            else
                parent::__construct('Unknown error.', 0, $e);
        } else {
            parent::__construct($msg, $id, $e);
        }
    }
}