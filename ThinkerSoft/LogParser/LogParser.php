<?php

namespace ThinkerSoft\LogParser;

use ThinkerSoft\Database\pgDatabase;

/**
 * Класс для импорта данных из лог-файлов в базу данных
 * @package ThinkerSoft\LogParser
 */
class LogParser
{
    const datePattern = '/^(?:2\d{3})[-\/.](0[1-9]|1[012])[-\/.](?:0[1-9]|[12]\d|3[01])$/';
    const timePattern = '/^(?:[0-1]\d|2[0-3])(?:[-:](?:[0-5]\d)){2}(?:\.\d+)?$/';
    const ipPattern = '/^(?:(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)$/';

    protected $_database;

    /**
     * Конструктор класса
     *
     * @param array $dbConfig массив, содержащий данные для подключения к БД
     */
    public function __construct(array $dbConfig)
    {
        // создаем экземпляр класса для работы с БД
        $this->_database = new pgDatabase($dbConfig);
    }

    /**
     * Деструктор класса
     */
    public function __destruct()
    {
        // если создан объект БД, то уничтожаем
        if (isset($this->_database))
            unset($this->_database);
    }

    /**
     * Парсит лог-файлы и заносит данные в БД
     *
     * @param string $log1 полный путь до лог-файла типа 1
     * @param string $log2 полный путь до лог-файла типа 2
     * @param bool $trunc ключ, определеяющий, очищать или нет таблицы назначения
     * @param bool $verb ключ, определеяющий, что необходим вывод информации
     * @throws LogParserException
     */
    public function parse($log1, $log2, $trunc = false, $verb = false)
    {
        // если, что либо передано в $log1, то
        if (!is_null($log1) && isset($log1) && (strlen($log1) > 0)) {
            // проверяем существет ли файл
            if (file_exists($log1)) {
                if ($verb) echo "Begin import log file type 1.\n";
                // если существуе, то парсим
                $this->parseLog1($log1, $trunc, $verb);
                if ($verb) echo "End import log file type 1.\n";
            } else {
                // иначе кидаем исключение
                throw new LogParserException(null, 201);
            }
        }

        if (!is_null($log2) && isset($log2) && (strlen($log2) > 0)) {
            if (file_exists($log2)) {
                if ($verb) echo "Begin import log file type 2.\n";
                $this->parseLog2($log2, $trunc, $verb);
                if ($verb) echo "End import log file type 2.\n";
            } else {
                throw new LogParserException(null, 202);
            }
        }
    }

    /**
     * Парсит лог-файл типа 1 и заносит данные в таблицу 'log1' БД
     *
     * @param string $log полный путь до лог-файла типа 1
     * @param bool $trunc ключ, определеяющий, очищать или нет таблицу назначения
     * @param bool $verb ключ, определеяющий, что необходим вывод информации
     * @throws LogParserException
     */
    private function parseLog1($log, $trunc = false, $verb = false)
    {
        if ($verb) echo "Open file.\n";
        // открываем файл на чтение
        $handle = fopen($log, "r");
        // если файл открыли, то
        if ($handle) {
            if ($verb) echo "Database connection.\n";
            // соединяемся с БД
            $this->_database->connect();
            // если необходима очистка таблицы
            if ($trunc) {
                if ($verb) echo "Truncate table \"log1\".\n";
                // передаем запрос на очистку
                $this->_database->query('truncate log1;');
            }
            $readLine = 0;
            $insertLine = 0;
            // читаем файл построчно
            while (($line = fgets($handle)) !== false) {
                $readLine++;
                // разбиваем строку
                $arr = explode('|', $line);
                // если количество переданных элементов больше либо равно 5, то
                if (count($arr) >= 5) {
                    // проверяем, соответствуют ли переданные данные заданым шаблонам
                    if (preg_match(self::datePattern, trim($arr[0])) &&
                        preg_match(self::timePattern, trim($arr[1])) &&
                        preg_match(self::ipPattern, trim($arr[2]))
                    ) {
                        // передаем запрос на добавление данных в таблицу
                        $this->_database->query(
                            'insert into log1 values(:log_datetime, :ip_address, :from_url, :to_url);',
                            array(
                                ':log_datetime' => trim($arr[0]) . ' ' . trim($arr[1]),
                                ':ip_address' => trim($arr[2]),
                                ':from_url' => trim($arr[3]),
                                ':to_url' => trim($arr[4])
                            )
                        );
                        $insertLine++;
                    }
                }
                if ($verb) echo "Read line: $readLine, insert line: $insertLine\r";
            }
            if ($verb) echo "\nClose database connection.\n";
            // закрываем соединение с БД
            $this->_database->disconnect();
            if ($verb) echo "Close file.\n";
            // закрываем файл
            fclose($handle);
        } else {
            // иначе бросаем исключение
            throw new LogParserException(null, 203);
        }
    }

    /**
     * Парсит лог-файл типа 2 и заносит данные в таблицу 'log2' БД
     *
     * @param string $log полный путь до лог-файла типа 2
     * @param bool $trunc ключ, определеяющий, очищать или нет таблицу назначения
     * @param bool $verb ключ, определеяющий, что необходим вывод информации
     * @throws LogParserException
     */
    private function parseLog2($log, $trunc = false, $verb = false)
    {
        if ($verb) echo "Open file.\n";
        $handle = fopen($log, "r");
        if ($handle) {
            if ($verb) echo "Database connection.\n";
            $this->_database->connect();
            if ($trunc) {
                if ($verb) echo "Truncate table \"log2\".\n";
                $this->_database->query('truncate log2;');
            }
            $readLine = 0;
            $insertLine = 0;
            while (($line = fgets($handle)) !== false) {
                $readLine++;
                $arr = explode('|', $line);
                if (count($arr) >= 3) {
                    if (preg_match(self::ipPattern, trim($arr[0]))) {
                        $this->_database->query(
                            'insert into log2 values(:ip_address, :browser_name, :os_name);',
                            array(
                                ':ip_address' => trim($arr[0]),
                                ':browser_name' => trim($arr[1]),
                                ':os_name' => trim($arr[2])
                            )
                        );
                        $insertLine++;
                    }
                }
                if ($verb) echo "Read line: $readLine, insert line: $insertLine\r";
            }
            if ($verb) echo "\nClose database connection.\n";
            $this->_database->disconnect();
            if ($verb) echo "Close file.\n";
            fclose($handle);
        } else {
            throw new LogParserException(null, 204);
        }
    }
}