<?php

namespace ThinkerSoft\LogParser;

use Exception;

/**
 * Класс исключений для LogParser
 * @package ThinkerSoft\LogParser
 */
class LogParserException extends Exception
{
    protected $errors = array(
        201 => "Log file type 1 not found.",
        202 => "Log file type 2 not found.",
        203 => "Can't open log file type 1.",
        204 => "Can't open log file type 2.",
    );

    function __construct($msg, $id = 0, $e = null)
    {
        if (($id > 0) && empty($msg)) {
            if (isset($this->errors[$id]))
                parent::__construct($this->errors[$id], $id, $e);
            else
                parent::__construct('Unknown error.', 0, $e);
        } else {
            parent::__construct($msg, $id, $e);
        }
    }
}