<?php

namespace ThinkerSoft\WebBackend;

use ThinkerSoft\Database\pgDatabase;

/**
 * Класс TestTask
 * Реализует методы, возвращающие данные для интерфейса
 * @package ThinkerSoft\WebBackend
 */
class TestTask
{
    protected $_database;

    /**
     * Конструктор класса
     *
     * @param array $dbConfig массив, содержащий данные для подключения к БД
     */
    public function __construct(array $dbConfig)
    {
        $this->_database = new pgDatabase($dbConfig);
    }

    /**
     * Деструктор класса
     */
    public function __destruct()
    {
        // если создан объект БД, то уничтожаем
        if (isset($this->_database))
            unset($this->_database);
    }

    /**
     * Возвращет данные для таблицы отображения логов
     *
     * @param array $filter массив, содержащий параметры фильтрации
     * @param array $sort массив, содержащий параметры сортировки
     * @return array
     */
    public function getDataStore(array $filter, array $sort)
    {
        // формируем запрос к БД
        $sql = 'select
                  A.ip_address,
                  A.browser_name,
                  A.os_name,
                  (select B.from_url from log1 B 
                   where B.ip_address=A.ip_address 
                   order by B.log_datetime limit 1
                  ) as first_url,
                  (select C.to_url from log1 C 
                   where C.ip_address=A.ip_address 
                   order by C.log_datetime desc limit 1
                  ) as last_url,
                  (select distinct count(D.to_url) 
                   from log1 D 
                   where D.ip_address=A.ip_address
                  ) as count_url
                from log2 A';

        // проверяем, что нам передали в фильтрах
        if (!is_null($filter) && !empty($filter) && is_array($filter)) {
            $where = array();
            foreach ($filter as $val) {
                // если есть фильтр по IP, то добавляем в массив
                if ($val['property'] === 'ip_address') {
                    array_push($where, $val['property'] . " like '"
                        . preg_replace(array('/[^0-9.]/i', '/\.+/i'), array('', '.'), $val['value']) . "%'");
                }
            }
            // если массив с фильтрами не пуст, то добавляем условие отбора в запрос
            if (count($where) > 0) {
                $sql .= ' where ' . join( ' and ', $where);
            }
        }

        // проверяем, что нам передали в сортировке
        if (!is_null($sort) && !empty($sort) && is_array($sort)) {
            $order = array();
            foreach ($sort as $val) {
                // если есть сортировка по браузеру или по ОС, то добавляем в массив
                if (($val['property'] === 'browser_name') || ($val['property'] === 'os_name')) {
                    array_push($order, $val['property'] . ' ' . $val['direction']);
                }
            }
            // если массив с сортровкой не пуст, то добавляем в запрос условия сортировки
            if (count($order) > 0) {
                $sql .= ' order by ' . join( ', ', $order);
            }
        }

        $sql .= ';';

        // коннектимся к базе и выполняем запрос
        $this->_database->connect();
        $result = $this->_database->query($sql);
        $this->_database->disconnect();

        // возвращаем результат
        return $result;
    }
}