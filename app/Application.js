Ext.define('TestTaskApp.Application', {
    extend: 'Ext.app.Application',

    name: 'TestTaskApp',

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
