Ext.define('TestTaskApp.model.Log', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.proxy.Ajax'
    ],

    fields: [{
        name: 'ip_address',
        type: 'string'
    },{
        name: 'browser_name',
        type: 'string'
    },{
        name: 'os_name',
        type: 'string'
    },{
        name: 'first_url',
        type: 'string'
    },{
        name: 'last_url',
        type: 'string'
    },{
        name: 'count_url',
        type: 'int'
    }],
    proxy: {
        type: 'ajax',
        actionMethod: 'GET',
        reader: {
            type: 'json'
        },
        url: 'http://127.0.0.1:8090/testtask.php'
    }
});