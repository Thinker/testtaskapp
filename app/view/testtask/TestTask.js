Ext.define('TestTaskApp.view.testtask.TestTask', {
    extend: 'Ext.grid.Panel',
    xtype: 'test-task-app',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.form.field.Text',
        'Ext.grid.filters.Filters',
        'Ext.grid.filters.filter.String',
        'Ext.toolbar.Paging',
        'Ext.window.MessageBox',
        'TestTaskApp.view.testtask.TestTaskController',
        'TestTaskApp.view.testtask.TestTaskModel'
    ],

    controller: 'TestTask',
    viewModel: 'TestTask',
    reference: 'TestTask',
    layout: 'fit',
    loadMask: true,
    bind: {
        title: '{name}',
        store: '{testTaskStore}'
    },
    plugins: [{
        ptype: 'gridfilters',
        menuFilterText: 'Фильтр'
    }],
    columns: {
        items: [{
            text: 'IP-адрес',
            dataIndex: 'ip_address',
            sortable: false,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Введите IP-адрес',
                    maskRe : /[0-9.]/
                }
            }
        },{
            text: 'Браузер',
            dataIndex: 'browser_name'
        },{
            text: 'ОС',
            dataIndex: 'os_name'
        },{
            text: 'Первый вход с URL',
            sortable: false,
            dataIndex: 'first_url'
        },{
            text: 'Последний URL',
            sortable: false,
            dataIndex: 'last_url'
        },{
            text: 'Уникальных URL',
            sortable: false,
            dataIndex: 'count_url'
        }],
        defaults: {
            hideable: false,
            flex: 1
        }
    },
    // dockedItems: [{
    //     xtype: 'pagingtoolbar',
    //     dock: 'bottom',
    //     displayInfo: true,
    //     bind: {
    //         store: '{testTaskStore}'
    //     }
    // }],
    emptyText: 'Нет данных для отображения',
    listeners: {
        beforeload: 'onBeforeLoad'
    }

});
