Ext.define('TestTaskApp.view.testtask.TestTaskController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.TestTask',

    requires: [
        'Ext.window.MessageBox'
    ],

    onLoadStore: function(store, recs, successful, op) {
        if (!successful) {
            var errMsg, err = op.getError();
            if (Ext.isString(err))
                errMsg = err;
            else if (Ext.isObject(err))
                errMsg = err.statusText;
            else
                errMsg = 'Неизвестная ошибка.';
            Ext.Msg.show({
                title: 'Ошибка',
                message: errMsg,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR,
                multiLine: true
            });
        }
    }
});
