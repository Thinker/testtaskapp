Ext.define('TestTaskApp.view.testtask.TestTaskModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.TestTask',

    requires: [
        'Ext.data.Store',
        'TestTaskApp.model.Log'
    ],

    data: {
        name: 'TestTask-LogViewer'
    },
    stores: {
        testTaskStore: {
            model: 'TestTaskApp.model.Log',
            remoteSort: true,
            remoteFilter: true,
            pageSize: 50,
            autoLoad: true,
            listeners: {
                load: 'onLoadStore'
            }
        }
    }
});
