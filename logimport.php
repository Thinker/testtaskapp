<?php

define('ROOT_PATH', __DIR__ . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($class_name) {
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
    $file = ROOT_PATH . $path . '.php';
    if (is_file($file)) {
        require_once($file);
    }
});

date_default_timezone_set('Europe/Moscow');

use ThinkerSoft\LogParser\ArgsParser;
use ThinkerSoft\LogParser\LogParser;

// массив, содержащий настройки подключения к БД
$dbConfig = array(
    '_hostname' => 'localhost',
    '_database' => 'testtask',
    '_username' => 'postgres',
    '_password' => ''
);

try {
    // парсим аргументы командной строки
    $argsParser = new ArgsParser();
    $argsParseParams = $argsParser->parse();
    unset($argsParser);

    // парсим логи
    $logParser = new LogParser($dbConfig);
    $logParser->parse($argsParseParams['log1'], $argsParseParams['log2'],
        $argsParseParams['truncate'], $argsParseParams['verbose']);
    unset($logParser);
} catch (Exception $ex) {
    $errMsg = $ex->getMessage();
    echo "\nERROR: $errMsg\n";
}
