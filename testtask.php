<?php

define('ROOT_PATH', __DIR__ . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($className) {
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    $file = ROOT_PATH . $path . '.php';
    if (is_file($file)) {
        require_once($file);
    }
});

date_default_timezone_set('Europe/Moscow');

use ThinkerSoft\WebBackend\TestTask;

// массив, содержащий настройки подключения к БД
$dbConfig = array(
    '_hostname' => 'localhost',
    '_database' => 'testtask',
    '_username' => 'postgres',
    '_password' => ''
);

// получаем, данные фильтра, переданные в запросе
$filters = isset($_GET['filter']) ? json_decode($_GET['filter']) : array();
$filter = array();
if (is_array($filters)) {
    foreach ($filters as $val) {
        if (is_object($val)) {
            if (property_exists($val, 'property') && property_exists($val, 'value')) {
                array_push($filter, array(
                    'property' => $val->property,
                    'value' => $val->value
                ));
            }
        }
    }
}

// получаем, данные сортировки, переданные в запросе
$sorts = isset($_GET['sort']) ? json_decode($_GET['sort']) : array();
$sort = array();
if (is_array($sorts)) {
    foreach ($sorts as $val) {
        if (is_object($val)) {
            if (property_exists($val, 'property') && property_exists($val, 'direction')) {
                array_push($sort, array(
                    'property' => $val->property,
                    'direction' => $val->direction
                ));
            }
        }
    }
}

try {
    // запрашиваем данные из БД
    $testTask = new TestTask($dbConfig);
    echo json_encode($testTask->getDataStore($filter, $sort));
    unset($testTask);
} catch (Exception $ex) {
    // в случае исключения, возвращаем текст сообщения об ошибке
    return array('success' => false, 'error' => $ex->getMessage());
}
